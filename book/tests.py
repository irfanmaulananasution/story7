from django.test import TestCase
from django.conf import settings
from importlib import import_module
from django.http import HttpRequest
from django.test import TestCase, Client
from django.urls import resolve
from django.utils import timezone

from .views import book

class BookUnitTest(TestCase):

    def test_book_page_exist(self):
        response = Client().get('/book/')
        self.assertEqual(response.status_code, 200)

    def test_url_not_found(self):
        response = Client().get('/book/heihei/')
        self.assertEqual(response.status_code, 404)

    def test_book_page_using_index_func(self):
        found = resolve('/book/')
        self.assertEqual(found.func, book)

    def test_book_page_uses_template(self):
        response = Client().get('/book/')
        self.assertTemplateUsed(response, 'book.html')

