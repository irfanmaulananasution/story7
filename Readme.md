[![pipeline status](https://gitlab.com/irfanmaulananasution/story7/badges/master/pipeline.svg)](https://gitlab.com/irfanmaulananasution/story7/commits/master)
[![coverage report](https://gitlab.com/irfanmaulananasution/story7/badges/master/coverage.svg)](https://gitlab.com/irfanmaulananasution/story7/commits/master)


#story7

### Description / Overview
this django app is a website to write a status, it is public
the purpose of this project is to make an accordion and a 2 theme website
access here : irfanmaulananasution.herokuapp.com

### Requirements 
general requirement :
- Python
- Django
- Git
others : check requirements.txt

### Installation / Build Instruction
to reuse this project do:
```
git clone https://gitlab.com/irfanmaulananasution/story7.git
```
go to the new project directory
```
pip install -r requirements.txt
```
and then customize it all you want.

### How To Run
- to run this in local. after you build the project do :
```
python manage.py runserver
```
- to run this in server do deploy the way you prefer.

### Author
- Irfan Maulana Nasution

### Changelog
see the changelog here : https://gitlab.com/irfanmaulananasution/story7/-/jobs
*since its a continuation project of previous project
see previous changelog here : https://gitlab.com/irfanmaulananasution/story6-2/-/jobs
